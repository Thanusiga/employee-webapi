﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Department
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Department Name")]
        [Display(Name = "Department Name")]
        public String departmentName { get; set; }

        // Navigation Properties
        public ICollection<Employee> Employees { get; set; }
    }
}
